﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Animator.Test
{
    public class Generator
    {
        private Point a;
        private Point b;
        private double length;
        private double angle;

        private double steepness = -0.5;

        public double Steepness
        {
            get { return steepness; }
            set { steepness = value; }
        }

        public Generator(Point start, Point finish)
        {
            a = start;
            b = finish;
            Point ab = new Point(b.X - a.X, b.Y - a.Y);
            length = Math.Sqrt(ab.X * ab.X + ab.Y * ab.Y);
            angle = Math.Acos(ab.X / length);
        }

        private PointF Generate(double t) // percent 
        {
            PointF res = new PointF(
                a.X + (float)(Math.Cos(angle) * length * t),
                a.Y + (float)(Math.Sin(angle) * length * t));
            return res;
        }

        private double Sigmoid(double x, double k)
        {
            return (k * x - x) / (2 * k * x - k - 1);
        }

        public PointF[] GenerateArray(int frames)
        {
            PointF[] res = new PointF[frames];
            
            for (int frame = 0; frame < frames; frame++)
                res[frame] = Generate(Sigmoid((double)frame/frames, steepness));

            res[frames - 1] = Generate(1);

            return res;
        }
    }
}